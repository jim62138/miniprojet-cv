var setupListeners = function(){ /* ici le setuplisteners pour abonnée mes elements */
	var boutonMenu = document.getElementById("bouton");
	boutonMenu.addEventListener("click",afficherMenu);
	afficherMenu(); /* ici j'ai du lancer ma fonction 1 fois car sinon ca marcher qua partir du deuxieme clic de souris */
	boutonMenu.addEventListener("mouseover",scrollup);
	
	var boutonContact = document.getElementById("boutonContact");
	boutonContact.addEventListener("click",scrolldown);
	
	var boutonContraste = document.getElementById("contraste");
	boutonContraste.addEventListener("click",changementdestyle);
}

function scrollup(){ /* cette fonction permet de scroller doucement */
	window.scrollTo({top: 0, behavior: 'smooth' });
}

function scrolldown(){ /* meme chose mais vers une autre position dans la page(en bas) */
	window.scrollTo({ top: 1050, behavior: 'smooth' });
}

function afficherMenu(){ /* cette fonction me permets de basculer mon menu entre afficher ou cacher (block ou none)*/
	var voir = document.getElementById("nav").style.display;
	if(voir === "none"){
	document.getElementById("nav").style.display = "block";
	}
	else{
		document.getElementById("nav").style.display = "none";
	}
}

function menu(x) { /* la c'est ma fonction qui sert au bouton menu en croix ou 3 ligne paralelles */
  x.classList.toggle("change");
}

function changementdestyle() {                  /* la j'ai galere mais ca sert a changer ma feuille de style pour le contraste */
    var baliseLink = document.getElementById("linkcss");
    var etat = baliseLink.href.endsWith("style.css"); /* si ca se finir par style.css je le recupere danns une variable */
    if (etat == true) {                         /* si vrais on le remplace par l'autre ici contraste.css */
        baliseLink.href="contraste.css";
    } else {
        baliseLink.href="style.css";
    }
}

window.addEventListener("load",setupListeners); /* ici attend que la page soit totalement charger pour charger le setupelistener déclarer en haut ici */
